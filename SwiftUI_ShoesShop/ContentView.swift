////  ContentView.swift
//  SwiftUI_ShoesShop
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
