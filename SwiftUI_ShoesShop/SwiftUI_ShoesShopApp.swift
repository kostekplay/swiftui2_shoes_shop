////  SwiftUI_ShoesShopApp.swift
//  SwiftUI_ShoesShop
//
//  Created on 15/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI_ShoesShopApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
